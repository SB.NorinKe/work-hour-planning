# Install Dependency
- npm install axios moment --save


# Add authentication header.
- Login into: [Tcube](tcube77.com)
- Open DevTools (F12) -> Network tab -> Copy Authorization token 
- Paste into index.js


# Run
- node index.js


# Alias 
alias workhour="node ~/app/workhour/index.js"


# Sample Image
![sample image](sample-image.png)