var axios = require('axios');
var moment = require('moment');


function getWorkHourConfig() {
  var fs = require('fs');
  return JSON.parse(fs.readFileSync('./other-note-records.json', 'utf8'));
}


function getAttendanceRecord() {
  const url = "https://tcube77.com/api/attendanceRecord/web/record/date/id?id=2798&date=$data";
  const currentYearMonth = moment(new Date()).format("yyyy-MM");
  const previousYearMonth = moment(new Date()).subtract(1, "months").format("yyyy-MM");

  const headers = {
    'Authorization': 'Bearer xxx'
  }

  /// Request data
  const requestData = {
    method: 'get',
    headers: headers
  };

  /// Prepared data
  return [
    axios({ ...requestData, url: url.replace("$data", currentYearMonth) }),
    axios({ ...requestData, url: url.replace("$data", previousYearMonth) })
  ];
}


function initData(record, otherNoteRecords, result) {
  /// Init result object and working date
  result[record.attendanceDate] = result[record.attendanceDate] || {}
  result[record.attendanceDate].attendanceDate = record.attendanceDate


  /// Set arrived and left hours
  if (record.status === '准时上班了' || record.status.startsWith("迟到")) {
    result[record.attendanceDate].arrivedAt = record.time
  } else if (record.status === '出勤' || record.status.startsWith("早退")) {
    result[record.attendanceDate].leftAt = record.time
  }

  /// If Today then set default leftAt
  if (moment(record.attendanceDate).isSame(new Date(), 'day')) {
    result[record.attendanceDate].leftAt = moment(new Date()).format("HH:mm:ss");
  }

  /// If we have some notes. eg, leaves, holiday, app error
  if (record.attendanceDate in otherNoteRecords) {
    const otherNoteRecord = otherNoteRecords[record.attendanceDate];
    // console.log(record.attendanceDate, k)


    if (otherNoteRecord.type === "app-error" && otherNoteRecord.leftAt !== undefined) {
      result[record.attendanceDate].leftAt = otherNoteRecord.leftAt;
    }

    if (otherNoteRecord.type === "leaves" || otherNoteRecord.type === "holiday") {
      result[record.attendanceDate].holiday = otherNoteRecord.hour;
      // result[record.attendanceDate].arrivedAt = moment("00:00:00", "HH:mm:ss");
      // result[record.attendanceDate].leftAt = moment("00:00:00", "HH:mm:ss");
    }

    result[record.attendanceDate].type = otherNoteRecord.type;
    result[record.attendanceDate].reason = otherNoteRecord.reason;
  }
}


function calculateWorkHour(record, result) {
  if (result[record.attendanceDate].arrivedAt !== undefined && result[record.attendanceDate].leftAt !== undefined) {
    // start time and end time
    var startTime = moment(result[record.attendanceDate].arrivedAt, 'HH:mm:ss');
    var endTime = moment(result[record.attendanceDate].leftAt, 'HH:mm:ss'); /// In case Today, we set current hour as left hours

    let hour = endTime.diff(startTime, 'hours'); /// 1h breaks
    let minute = endTime.diff(startTime, 'minutes') % 60;
    let second = endTime.diff(startTime, 'seconds'); /// For 1h break


    /// Minus 1h break time
    if (
      /// if you come before 12:00:00, then we will minus 1h for the break
      moment(result[record.attendanceDate].arrivedAt, 'HH:mm:ss').isBetween(moment('00:00:00', 'HH:mm:ss'), moment('12:00:00', 'HH:mm:ss')) &&

      /// If current time is after 12:00:00 then we minus 1h. If we don't put this condition, we will get -1h on Monday morning.
      moment(result[record.attendanceDate].leftAt, 'HH:mm:ss').isAfter(moment('13:00:00', 'HH:mm:ss')
      )
    ) {
      hour -= 1;
      second -= 3600;
    }

    result[record.attendanceDate].workAsString = `${hour}h ${minute}m`;
    result[record.attendanceDate].workHourInSecond = second;
    result.totalWorkHourInSeconds += second;
  }
}


axios.all(getAttendanceRecord())
  .then(axios.spread((currentMonthAttendanceRecordResponse, previousMonthAttendanceRecordResponse) => {
    var data = currentMonthAttendanceRecordResponse.data.data.internalRecords;
    var previousData = previousMonthAttendanceRecordResponse.data.data.internalRecords;
    var result = { totalWorkHourInSeconds: 0 };
    const bothCurrentAndPreviousMonth = [...previousData, ...data];
    const otherNoteRecords = getWorkHourConfig();



    /// Print summary
    console.log("-------------------------------------------------------------------------------------------------");
    console.log(`\t\t\t\t\t => Previous Month <=`);
    console.log("-------------------------------------------------------------------------------------------------\n");

    for (let key in bothCurrentAndPreviousMonth) {
      let record = bothCurrentAndPreviousMonth[key];

      /// Filter current week only
      if (moment(record.attendanceDate).isSame(new Date(), 'month')) {
        continue;
      }

      initData(record, otherNoteRecords, result);
      calculateWorkHour(record, result);
    }


    /// Get record and put it into week (5, 4, 3, 2 ,1). So we can easily display it by week
    for (let key in result) {
      let record = result[key];

      if (record.leftAt !== undefined) {
        let week = moment(key, "yyyy-MM-DD").week();
        result[week] = [...result[week] || []]
        result[week].push(record)
      }
    }


    /// Print
    var totalWeeklyWorkhour = {};
    let totalMontlyWorkhour = 0;
    for (let ind = 5; ind > 0; ind--) {
      let records = result[ind];

      if (records !== undefined) {
        console.log(`Date (${ind})`.padEnd(30), "From".padEnd(20), "To".padEnd(20), "Hours".padEnd(20), "Notes".padEnd(20));

        for (let key in records) {
          let record = records[key];
          let dayOfTheWeekAsString = moment(record.attendanceDate).format('dddd');
          const date = `${record.attendanceDate} - ${dayOfTheWeekAsString}`.padEnd(30);
          const from = (record.arrivedAt || "").padEnd(20);
          const to = (record.leftAt || "").padEnd(20);
          const hours = record.workAsString.padEnd(20);
          const notes = (record.reason || "-").padEnd(20);
          totalWeeklyWorkhour[ind] = (totalWeeklyWorkhour[ind] || 0) + record.workHourInSecond;
          console.log(date, from, to, hours, notes)
        }


        totalMontlyWorkhour += totalWeeklyWorkhour[ind];


        /// Print summary
        console.log("----------------------         ---------");
        console.log(`=> Total Weekly Hours:         ${parseInt(totalWeeklyWorkhour[ind] / 3600)}h ${parseInt(totalWeeklyWorkhour[ind] % 3600 / 60)}m`);
        console.log("----------------------         ---------\n");
      }
    }

    /// Print summary
    console.log("-------------------------------------------------------------------------------------------------");
    console.log(`\t\t\t\t=> Total Monthly Hours: ${parseInt(totalMontlyWorkhour / 3600)}h ${parseInt(totalMontlyWorkhour % 3600 / 60)}m <=`);
    console.log("-------------------------------------------------------------------------------------------------");
    console.log("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");



    /// CURRENT MONTH
    /// Reset result for the current month    
    var result = { totalWorkHourInSeconds: 0 };

    /// Prepare Data
    for (let key in bothCurrentAndPreviousMonth) {
      let record = bothCurrentAndPreviousMonth[key];

      /// Filter current week only
      if (!moment(record.attendanceDate).isSame(new Date(), 'week')) {
        continue;
      }

      initData(record, otherNoteRecords, result);
      calculateWorkHour(record, result);
    }


    /// Print work hour each day
    console.log("Date".padEnd(30), "From".padEnd(20), "To".padEnd(20), "Hours".padEnd(20), "Notes".padEnd(20));
    for (let key in result) {
      let record = result[key];
      const testCases = []

      if (record.attendanceDate !== undefined) {
        let dayOfTheWeekAsString = moment(record.attendanceDate).format('dddd');
        const date = `${record.attendanceDate} - ${dayOfTheWeekAsString}`.padEnd(30);
        const from = (record.arrivedAt || "").padEnd(20);
        const to = (record.leftAt || "").padEnd(20);
        const hours = record.workAsString.padEnd(20);
        const notes = (record.reason || "-").padEnd(20);

        console.log(date, from, to, hours, notes)
      }
    }

    /// Minus holiday and leaves from total work hours
    const totalHoliday = Object.keys(result).filter(a => result[a].holiday !== undefined).map(a => result[a].holiday);
    const totalWorkHourRequired = 43 - (totalHoliday.length === 0 ? 0 : totalHoliday.reduce((a, b) => a + b));


    /// Print summary
    console.log("\n\n---------- Summary ----------");
    console.log(`=> Total Hour Required:      ${totalWorkHourRequired}h`);
    console.log(`=> Total Worked Hours:       ${parseInt(result.totalWorkHourInSeconds / 3600)}h ${parseInt(result.totalWorkHourInSeconds % 3600 / 60)}m`);
    console.log(`=> Additional Hours Needed:  ${parseInt(totalWorkHourRequired - (result.totalWorkHourInSeconds / 3600))}h ${parseInt(60 - (result.totalWorkHourInSeconds % 3600 / 60))}m`)


    /// Print Planning
    // - Calculate how many day from Today until: Friday, Saturday, Sunday
    let todayUntilFriday = 6 - moment(new Date()).format('d');
    let todayUntilSaturday = 7 - moment(new Date()).format('d');
    let todayUntilSunday = 8 - moment(new Date()).format('d');

    /// I am not counting today work hour for the planning bcz, eg. I need to work 5h in today for this week and it's Monday. Which mean i need to work 1h each day. But now I have worked for half an hour already so it will make calculation display invalid as it will use 5h 30 divid by 5.
    let today = moment(new Date()).format('YYYY-MM-DD');
    let totalWorkHourInSecondToday = result[today].workHourInSecond;
    let totalWorkHourInSecondWithoutCountingToday = result.totalWorkHourInSeconds - totalWorkHourInSecondToday;

    // - How many work hour required for each day until: Friday, Saturday, Sunday 
    let workHourRequiredUntilFriday = (totalWorkHourRequired - totalWorkHourInSecondWithoutCountingToday / 3600) / todayUntilFriday;
    let workHourRequiredUntilSaturday = (totalWorkHourRequired - totalWorkHourInSecondWithoutCountingToday / 3600) / todayUntilSaturday;
    let workHourRequiredUntilSunday = (totalWorkHourRequired - totalWorkHourInSecondWithoutCountingToday / 3600) / todayUntilSunday;

    console.log('\n\n--------- Planning ----------')
    console.log(`=> Work Until Friday Planning:    ${parseInt(workHourRequiredUntilFriday)}h ${parseInt(workHourRequiredUntilFriday * 60 % 60)}m`.padEnd("42") + `=> ${result[today].arrivedAt} - ${moment(result[today].arrivedAt, 'HH:mm:ss').add((workHourRequiredUntilFriday) + 1 /*1h break*/, 'hours').format('HH:mm:ss')}`)
    console.log(`=> Work Until Saturday Planning:  ${parseInt(workHourRequiredUntilSaturday)}h ${parseInt(workHourRequiredUntilSaturday * 60 % 60)}m`.padEnd("42") + `=> ${result[today].arrivedAt} - ${moment(result[today].arrivedAt, 'HH:mm:ss').add((workHourRequiredUntilSaturday) + 1 /*1h break*/, 'hours').format('HH:mm:ss')}`)
    console.log(`=> Work Until Sunday Planning:    ${parseInt(workHourRequiredUntilSunday)}h ${parseInt(workHourRequiredUntilSunday * 60 % 60)}m`.padEnd("42") + `=> ${result[today].arrivedAt} - ${moment(result[today].arrivedAt, 'HH:mm:ss').add((workHourRequiredUntilSunday) + 1 /*1h break*/, 'hours').format('HH:mm:ss')}`)


    /// Print calculator.net
    var url = "https://www.calculator.net/time-card-calculator.html?label1=Monday&from11=%from11&to11=%to11&rate11=1:00&label2=Tuesday&from12=%from12&to12=%to12&rate12=1:00&label3=Wednesday&from13=%from13&to13=%to13&rate13=1:00&label4=Thursday&from14=%from14&to14=%to14&rate14=1:00&label5=Friday&from15=%from15&to15=%to15&rate15=1:00&label6=Saturday&from16=%from16&to16=%to16&rate16=1:00&label7=Sunday&from17=%from17&to17=%to17&rate17=1:00&rate17=&crt=&repheader=&repnote=&payrate=35&oth3=8&oth4=40&oth1=8&cpo=w&oth2=40&otrate=1.5&printit=0&x=50&y=22";
    for (let key in result) {
      let record = result[key];
      let dayOfTheWeekAsNo = moment(record.attendanceDate).format('d');

      if (record.attendanceDate !== undefined) {
        url = url.replace("%from1" + dayOfTheWeekAsNo, moment(record.arrivedAt, "HH:mm:ss").format("hh:mma"));
        url = url.replace("%to1" + dayOfTheWeekAsNo, moment(record.leftAt, "HH:mm:ss").format("hh:mma"));
      }
    }

    console.log("\n\n-------- Calculator ---------");
    console.log(url);














    // console.log(result)
  }))
  .catch(function (error) {
    console.log(error);
  });
